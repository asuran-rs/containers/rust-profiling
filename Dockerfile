ARG BASE_IMAGE=opensuse/tumbleweed:latest
FROM $BASE_IMAGE

# Update and install packages
RUN zypper update; \
        zypper -n dist-upgrade; \
        zypper -n in curl wget fish shadow cmake git git-lfs libopenssl-devel gcc-c++ \
        perf valgrind lldb gdb oprofile; \
        zypper -n in -t pattern devel_C_C++; \
        zypper clean --all

# Install rust, add latest nightly toolchain as well
RUN mkdir -p /tmp/rustup; cd /tmp/rustup; \
        wget https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init; \
        chmod +x ./rustup-init; ./rustup-init -y; \
        cd /; rm -r /tmp/rustup ; \
        /root/.cargo/bin/rustup component add rustfmt clippy; \
        /root/.cargo/bin/rustup toolchain add nightly

# Add rust-unmangle
ADD https://raw.githubusercontent.com/Yamakaky/rust-unmangle/master/rust-unmangle /usr/bin/rust-unmangle
RUN chmod +x /usr/bin/rust-unmangle

# Add flamegraph
RUN git clone https://github.com/brendangregg/FlameGraph.git /opt/flamegraph; \
        chmod +x /opt/flamegraph/*.pl

# Initialize git-lfs
RUN git lfs install

# Copy shell config
COPY config.fish /root/.config/fish/config.fish

# Set root's shell to fish
RUN chsh -s /usr/bin/fish root
WORKDIR /root
ENTRYPOINT /usr/bin/fish

